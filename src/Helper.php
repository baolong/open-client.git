<?php

// +----------------------------------------------------------------------
// | Library for ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2022 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: https://gitee.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 仓库地址 ：https://gitee.com/zoujingli/ThinkLibrary
// | github 仓库地址 ：https://github.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\openClient;

use think\App;
use think\Container;
use think\facade\Config;

/**
 * 控制器挂件
 * Class Helper
 * @package think\admin
 */
class Helper extends \think\admin\Helper
{


    public $apiDbConfig;

    public $database;

    public $connect;

    public $dynamicMark;

    /**
     * Helper constructor.
     * @param App $app
     * @param Controller $class
     */
    public function __construct(App $app, Controller $class) {
       parent::__construct($app,$class);

        // 获取服务器数据库链接对象
        $this->apiDbConfig =  Api::OpenSerDb()->getConfig();
        $this->database = Config::get('database'); //获取本地数据库配置参数
        $this->dynamicMark = 'DynamicSqlMark';
        $this->connect($this->apiDbConfig['default']);
    }

    /**
     * 设置数据链接
     * @param string|null $name
     * @return $this
     */
    public function connect(string $name = null):Helper {
        if ($name){
            $this->connect = $name;
            $this->database['connections'][$this->dynamicMark] =  $this->apiDbConfig['connections'][$name]; //获取服务器数据库配置
            Config::set($this->database, 'database'); // 动态设定数据库链接
        }
        return $this;
    }

    /**
     * 获取数据库查询对象
     * @param $query
     * @return mixed|\think\Db
     */
    public function buildOpenQuery($query)
    {
        return is_string($query) ? $this->app->db->connect($this->dynamicMark)->name($query) : $query;
    }

}